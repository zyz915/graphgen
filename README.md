## What is this repository for? ##

This repository is for creating random graphs based on given degree distribution

* currently it supports only power-law degree distribution (using the erased configuration model)

## Installation ##

The compiler requires the following environments:

1. [cmake](https://cmake.org/)
2. a c++ compiler (C++11 compatible)

The following steps build the compiler:

```
$ cd $(top)
$ cmake .
$ make
```

The graph generator is a single executable `./gen`.

## Contact ##

Yongzhe Zhang (Ph.D. student)  
The Graduate University of Advanced Studies (SOKENDAI), Japan  
National Institute of Informatics  
Email: zyz915@gmail.com