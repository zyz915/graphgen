#include <algorithm>
#include <cstdio>

#include "graph.h"

using namespace std;

void Graph::add_edge(int u, int v, int w) {
	if (u != v) {
		edges_.push_back(Edge(u, v, w));
		if (!directed_)
			edges_.push_back(Edge(v, u, w));
	}
}

void Graph::dump(FILE *fp) {
	if (edges_.empty()) return;
	sort(edges_.begin(), edges_.end());
	int ptr = 0, tmp;
	for (int id = 0; id < numv_; id++) {
		vector<pair<int, int> > sel;
		for (tmp = ptr; tmp < edges_.size() && edges_[tmp].u == id; tmp++)
			if (tmp == ptr || edges_[tmp].v != edges_[tmp - 1].v)
				sel.push_back(make_pair(edges_[tmp].v, edges_[tmp].w));
		fprintf(fp, "%d %zu", id, sel.size());
		if (weighted_) {
			for (int i = 0; i < sel.size(); i++)
				fprintf(fp, " %d %d", sel[i].first, sel[i].second);
		} else {
			for (int i = 0; i < sel.size(); i++)
				fprintf(fp, " %d", sel[i].first);
		}
		fprintf(fp, "\n");
		ptr = tmp;
	}
}