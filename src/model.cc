#include <algorithm>
#include <cmath>
#include <cstdlib>

#include "model.h"

using namespace std;

static double rand_dbl(double range = 1.0) {
	return rand() * range / RAND_MAX;
}

static double get_k(int n, double avg_deg) {
	double lo = 1.0, hi = 10.0, s1, s2;
	for (int it = 0; it < 30; it++) {
		double mid = (lo + hi) * 0.5;
		s1 = s2 = 0;
		for (int i = 1; i < n; i++) {
			s1 += pow(i, -mid);
			s2 += pow(i, 1 - mid);
		}
		double deg = s2 / s1;
		if (avg_deg < deg)
			lo = mid;
		else
			hi = mid;
	}
	return lo;
}

Graph erased_configuration_model(int n, double avg_deg, 
		bool directed, int range, int limit) {
	// add 2 edges at one time for undirected graph
	if (!directed) avg_deg *= 0.5;
	if (limit <= 0 || limit > n) limit = n;
	double k = get_k(limit, avg_deg);
	vector<double> cdf(n);
	cdf[0] = 0;
	for (int i = 1; i < limit; i++)
		cdf[i] = cdf[i - 1] + pow(i, -k);
	double S = cdf[limit - 1];
	vector<pair<double, int> > ran;
	for (int i = 0; i < n; i++)
		ran.push_back(make_pair(rand_dbl(S), i));
	sort(ran.begin(), ran.end());
	vector<int> labels;
	int ptr = 0;
	for (int i = 0; i < ran.size(); i++) {
		while (ran[i].first > cdf[ptr]) ptr++;
		for (int j = 0; j < ptr; j++)
			labels.push_back(ran[i].second);
	}
	random_shuffle(labels.begin(), labels.end());
	Graph ret(n, directed, range > 0);
	for (int i = 0; i + 1 < labels.size(); i += 2) {
		int w = (range > 0 ? rand() % range + 1 : 0);
		ret.add_edge(labels[i], labels[i + 1], w);
	}
	return ret;
}
