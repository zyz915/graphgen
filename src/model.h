#ifndef MODEL_H_
#define MODEL_H_

#include "graph.h"

Graph erased_configuration_model(int n, double avg_deg, 
		bool directed, int range = 0, int limit = 0);

#endif /* MODEL_H_ */