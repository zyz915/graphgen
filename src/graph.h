#ifndef GRAPH_H_
#define GRAPH_H_

#include <cstdio>
#include <vector>

class Graph {
protected:
	struct Edge {
		int u, v, w;

		Edge() {}
		Edge(int u, int v, int w) : u(u), v(v), w(w) {}

		bool operator<(const Edge &a) const {
			return (u != a.u ? u < a.u : (v != a.v ? v < a.v : w < a.w));
		}
	};

	int numv_;
	bool directed_, weighted_;
	std::vector<Edge> edges_;

public:
	Graph(int n, bool directed = false, bool weighted = false) : 
			numv_(n), directed_(directed), weighted_(weighted) {}

	// copy constructor
	Graph(Graph &g) {
		numv_ = g.numv_;
		directed_ = g.directed_;
		weighted_ = g.weighted_;
		edges_ = g.edges_;
	}

	// move constructor
	Graph(Graph &&g) {
		numv_ = g.numv_;
		directed_ = g.directed_;
		weighted_ = g.weighted_;
		edges_.swap(g.edges_);
	}

	void add_edge(int x, int y, int w = 0);
	void dump(FILE *fp);
};

#endif /* GRAPH_H_ */