#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "model.h"

static int n; // number of vertices
static double avg; // averaged vertex degree
static bool directed = 0; // directed or not
static int range = 0; // range of edge (0 = no edge)
static int limit = 0; // maximum allowd vertex degree

int usage(const char *prog) {
	fprintf(stderr, "\n  usage: %s n d t w l\n\n", prog);
	fprintf(stderr, "    where each variable means:\n");
	fprintf(stderr, "      n :: int         number of vertices in the graph\n");
	fprintf(stderr, "      d :: double      averaged vertex (in/out-)degree\n");
	fprintf(stderr, "      t :: {0, 1}      0 = undirected, 1 = directed\n");
	fprintf(stderr, "      w :: int         range of edge weight is {1 .. w}\n");
	fprintf(stderr, "      l :: int         limitation of maximum degree\n");
	return 0;
}

int main(int argc, char *argv[]) {
	if (argc < 3 || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))
		return usage(argv[0]);
	if (argc > 2) {
		n = atoi(argv[1]);
		avg = atof(argv[2]);
		if (argc > 3)
			directed = atoi(argv[3]);
		if (argc > 4)
			range = atoi(argv[4]);
		if (argc > 5)
			limit = atoi(argv[5]);
	}
	Graph g = erased_configuration_model(n, avg, directed, range, limit);
	g.dump(stdout);
}